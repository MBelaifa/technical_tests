// Programme DateOfBirth Java pour trouver ou estimer votre âge à partir de votre date de naissanceimport org.joda.time.DateTime;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.Scanner;

// Début de la classe publique DateOfBirth
public class DateOfBirth {
    // Début de la méthode publique statique vide main(String[] args)
    public static void main(String[] args) throws IOException {
        try {
            // Entrer la date de votre naissance
            System.out.print("Saisir votre date de naissance en insérant des espaces entre chaque valeur " +
                    "[jour mois année] | (format de la date => jj MM aaaa) : ");
            Scanner s = new Scanner(System.in);
            String firstdate = s.nextLine();
            /* Entrer la date du jour ou une date future pour recevoir votre âge actuel ou une estimation
            (une date avant votre naissance peut-être entrer, vous recevrez un calcul chronologique
            inversé donc à priori une valeur négative en année(s)). */
            System.out.print("Saisir la date du jour pour connaitre votre age ou une date future pour estimer votre âge en insérant " +
                    "des espaces entre chaque valeur [jour mois année] | (format de la date => jj MM aaaa) : ");
            String seconddate = s.nextLine();
            // Formatter les deux dates (firstdate et seconddate)
            DateTimeFormatter dateStringFormat = DateTimeFormat
                    .forPattern("dd MM yyyy");
            DateTime firstTime = dateStringFormat.parseDateTime(firstdate);
            DateTime secondTime = dateStringFormat.parseDateTime(seconddate);
            // Calculer l'intervalle d'année(s) entre les deux dates formatées (firstTime et secondTime)
            Years years = Years.years(Years.yearsBetween(firstTime, secondTime).getYears());
            System.out.println("Votre âge est (avec la date du jour) ou votre âge sera " +
                    "(avec une date future) : " + years + "ears");
            // Message d'accomplissement et de réussite du programme DateOfBirth
            System.out.println("L'âge que vous avez actuellement, ou votre âge futur vous a été transmis.");
            /* Ici, Nous pourrions paramétrer les messages d'accompagnement et d'échec du programme EvenNumbers
            en fonction de la problématique rencontrer par l'utilisateur ou le programme DateOfBirth */
        } catch(Exception echec) {
            // Messages d'accompagnement et d'échec du programme DateOfBirth
            System.out.println("Problème d'exécution du programme DateOfBirth !!! Merci de bien vouloir vérifier " +
                    "le format des deux dates que vous avez inséré dans le programme(...réessayer et valider vos saisies !)");
            System.out.println("Le programme DateOfBirth à échoué, veuillez relancer le programme " +
                    "pour une nouvelle tentative et tenir informer le gestionnaire du programme, dans le cas échéant !!!!!");
        } finally {
            // Message de fermeture du programme DateOfBirth
            System.out.println("Veuillez fermer et quitter le programme DateOfBirth !");
        }
    }// Fin de la méthode publique statique vide main(String[] args)
}// Fin de la classe publique DateOfBirth
