// Programme EvenNumbers Java pour trouver la liste des nombres pairs allant de 1 à un nombre aléatoire entré par l'utilisateur
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// Début de la classe publique EvenNumbers
public class EvenNumbers {
    // Début de la méthode publique statique vide main(String[] args)
    public static void main(String[] args) throws IOException {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            // Entrer la valeur aléatoire (nombre positif entier ou décimal)
            System.out.println("Saisir une valeur aléatoire positive entière ou décimale de votre choix ! " +
                    "Appuyer sur votre clavier numérique, c'est à vous :");
            double nombre = 0;
            try {
                /* Transformation de votre nombre éventuellement positif décimal en nombre positif entier
                (nombre décimal arrondi à la valeur inférieur) */
                nombre = Double.parseDouble(in.readLine());
                // Pour récapituler votre valeur aléatoire positive
                System.out.println("la valeur aléatoire positive entière ou décimale" +
                        " que vous avez entré est : " + nombre);
                int n = (int) nombre;
                // Laïs récapitulatif pour afficher votre valeur aléatoire positive et la liste de valeurs pairs
                System.out.println("Les nombres pairs allant de 1 à votre valeur positive " +
                        "aléatoire entière ou décimale " + n + " sont : ");
                for (int i = 1; i <= n; i++) {
                    // Si nombre%2 == 0 cela signifie que c'est un nombre pair
                    if (i % 2 == 0) {
                        // Liste de valeurs pairs allant de 1 à votre nombre positif aléatoire
                        System.out.print(i + " ");
                        // Message d'accomplissement et de réussite du programme EvenNumbers
                        /* System.out.println("La valeur aléatoire entière ou décimale que vous avez entré," +
                           " vous a été transmise avec sa liste de valeurs positifs pairs"); */
                    }
                }
                /* Ici, Nous pourrions paramétrer les messages d'accompagnement et d'échec du programme EvenNumbers
                en fonction de la problématique rencontrer par l'utilisateur ou le programme EvenNumbers */
            } catch (Exception echec_1) {
                // Messages d'accompagnement et d'échec du programme EvenNumbers
                System.out.println("Problème d'exécution du programme EvenNumbers !!! " +
                        "Merci de bien vouloir vérifier " +
                        "le format de la valeur aléatoire que vous avez inséré dans le programme" +
                        "(...réessayer et valider vos saisies !)");
                System.out.println("Le programme EvenNumbers à échoué, veuillez relancer le programme " +
                        "pour une nouvelle tentative et tenir informer le gestionnaire du programme," +
                        " dans le cas échéant !!!!!");
            } finally {
                // Message de fermeture du programme EvenNumbers
                // System.out.println("Veuillez fermer et quitter le programme EvenNumbers !");
                System.out.println("Jusqu'ici, le programme EvenNumbers s'est déroulé avec ou sans échec !!!");
            }
            /* Ici, Nous pourrions paramétrer les messages d'accompagnement et d'échec du programme EvenNumbers
            en fonction de la problématique rencontrer par l'utilisateur ou le programme EvenNumbers */
        } catch (Exception echec_2) {
            // Messages d'accompagnement et d'échec du programme EvenNumbers
            System.out.println("Problème d'exécution du programme EvenNumbers !!! Merci de bien vouloir vérifier " +
                    "le format des deux valeurs hauteur et rayon du cône que vous avez inséré dans le programme" +
                    "(...réessayer et valider vos saisies !)");
            System.out.println("Le programme EvenNumbers à échoué, veuillez relancer le programme " +
                    "pour une nouvelle tentative et tenir informer le gestionnaire du programme," +
                    " dans le cas échéant !!!!!");
        } finally {
            // Message de fermeture du programme EvenNumbers
            System.out.println("Veuillez fermer et quitter le programme EvenNumbers !");
        }
    }// Fin de la méthode publique statique vide main(String[] args)
}// Fin de la classe publique EvenNumbers
