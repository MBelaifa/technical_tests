# DHCP_IPv4

Soit une machine fonctionnant sous Windows. L'adressage est de type
DHCP (en IPv4).

Cette machine n'a pas accès au réseau (d'après l'utilisateur).

ipconfig /all indique l'adresse IP 169.254.21.35/16

## Quelles sont les causes possibles du problème ?

Pour qu’un ordinateur accède à Internet via un réseau, il a besoin d’une adresse IP valide. Le moyen le plus simple de s’assurer que cela se produit de manière transparente consiste à utiliser le protocole DHCP (Dynamic Host Configuration Protocol), qui est un paramètre qui permet au routeur d’attribuer automatiquement une adresse IP à chaque périphérique du réseau.

Lorsqu’un ordinateur Windows n’est pas en mesure de communiquer avec le serveur DHCP, ce que l’on appelle l’adressage IP privé automatique (APIPA) entre en jeu. Il attribue à l’ordinateur une adresse IP commençant par 169.254. Ces adresses IP ne sont utiles que sur les réseaux locaux, pas sur Internet.

Sans communication entre l’ordinateur et le serveur DHCP, et tant que l’ordinateur a une adresse IP 169, il ne peut pas se connecter à Internet. C’est pourquoi la solution à ce problème consiste à s’assurer que votre ordinateur et le serveur DHCP peuvent communiquer. Lorsque cela se produit, le problème se résout de lui-même.

En savoir plus : [http://binaire-life.com/reseaux/dou-vient-ladresse-169-254-x-x/]

## Comment réagissez-vous à ces différents cas ?

Pour corriger une erreur où votre ordinateur a une adresse IP non valide qui commence par 169, vous devez faire en sorte que le périphérique réseau de votre ordinateur soit en mesure de communiquer avec votre matériel réseau.

Selon la raison pour laquelle vous rencontrez cette erreur, vous pourrez peut-être le faire en réinitialisant le matériel réseau, en demandant au périphérique réseau de l’ordinateur de demander une nouvelle adresse IP ou en modifiant certains paramètres du routeur.

### Solution 1 - Mettez le matériel réseau sous tension

Éteignez et débranchez votre modem et votre routeur, puis rebranchez les deux périphériques. Lorsque le matériel réseau démarre la sauvegarde et que votre ordinateur tente de se reconnecter au réseau, il peut être en mesure d’obtenir une adresse IP valide.

### Solution 2 - Utilisez l’utilitaire de résolution des problèmes de mise en réseau Windows

Ce processus automatisé prend en charge la plupart des problèmes de réseau, y compris ceux qui empêchent un ordinateur d’obtenir une adresse IP valide.

### Solution 3 - Demandez une nouvelle adresse IP

C’est un peu plus compliqué car vous devez ouvrir une invite de commandes et entrer une série de commandes. Dans la plupart des cas, cela permet à l’ordinateur d’obtenir une adresse IP valide.

#### Réinitialiser WINSOCK et la pile IP :

Taper la commande qui suit dans votre invite de commandes Cmd.exe ou dans votre Shell (en tant qu'administrateur) :

- netsh winsock reset catalog
- netsh int ipv4 reset reset.log
- netsh int ipv6 reset reset.log
- Redémarrer votre machine

#### Réinitialiser le protocole TCP/IP :

Taper la commande qui suit dans votre invite de commandes Cmd.exe ou dans votre Shell (en tant qu'administrateur):

- netsh int ip reset reset_tcp_ip.txt
- Redémarrer votre machine

### Solution 4 - Vérifiez les paramètres DHCP dans le routeur

Un routeur peut attribuer des adresses IP de deux manières. Soit le routeur attribue dynamiquement une adresse IP unique à chaque périphérique, sans aucune entrée de votre part, soit vous devez attribuer manuellement une adresse IP statique unique à chaque périphérique.

DHCP est le paramètre qui permet à un routeur d’attribuer des adresses IP de manière dynamique. Si ce paramètre est désactivé et que vous n’avez pas défini d’adresse IP statique pour l’ordinateur, vous ne pourrez pas accéder à Internet.

### Solution 5 - Désactivez le routeur

Dans certains cas, vous pouvez résoudre ce type de problème en désactivant le périphérique réseau, puis en le réactivant, ou en désinstallant et en réinstallant le pilote. Il s’agit de processus similaires qui nécessitent tous deux que vous accédiez au Gestionnaire de périphériques Windows.

#### Astuces - TIPS

Pour atteindre votre invite de commandes sous Windows, taper la commande qui suit dans la barre de recherche Windows :

- cmd.exe taper sur entrée

Pour atteindre votre Utilitaires de résolution des problèmes sous Windows, taper la commande qui suit dans la barre de recherche Windows :

- Utilitaires de résolution des problèmes taper sur entrée ou bien,
- Appuyez sur Win + I pour lancer l'application Paramètres et l'utilitaire de résolution des problèmes spécifiques à vos dysfonctionnements

Pour atteindre votre gestionnaire de périphériques sous Windows, taper la commande qui suit dans la barre de recherche Windows :

- devmgmt.msc taper sur entrée

Pour atteindre la fenêtre de connection réseau, taper la commande qui suit dans la barre de recherche Windows :

- ncpa.cpl taper sur entrée
