#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""Volume d'un cône."""
# Fichier : cone_volume.py
# Auteur : Moussa Belaifa

# Ce script gère le calcul du volume d’un cône droit à l’aide
# d’une hauteur et d’un rayon renseignés par l’utilisateur.
# Unités utilisées pour le calcul : le mètre et le mètre cube

# Imports
# from math import pi

# Programme principal -----------------------------------------------

# Sans importation en considérant que pi = 3,14
pi = 3.14

# Entrer la valeur du rayon du cône avec l'unité en mètre.
# Valeur stockée dans la variable rayon.
rayon = float(input("Rayon du cône(en mètre): "))

# Entrer la valeur de la hauteur du cône avec l'unité en mètre.
# Valeur stockée dans la variable hauteur.
hauteur = float(input("Hauteur du cône(en mètre): "))

# Calcul du volume du cône avec l'aide de la formule adéquate.
# Valeur stockée dans la variable volume.
volume = (pi*rayon*rayon*hauteur)/3.0

# Imprime le volume du cône avec l'unité en mètre cube.
print("Volume du cône = {} m3".format(volume))
