#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""Quel âge avez-vous?"""
# Fichier : date_of_birth.py
# Auteur : Moussa Belaifa

# Ce script gère si l'anniversaire est passé.
# Format de votre date de naissance : jj/mm/aaaa

# Imports
from datetime import datetime

# Programme principal -----------------------------------------------

dn=input('Votre date de naissance ? (format jj/mm/aaaa) :')
dn=dn.split('/');jn=int(dn[0]);mn=int(dn[1]);an=int(dn[2])
da=datetime.now()

if mn<da.month or (mn==da.month and jn<=da.day):
    age=da.year-an
else:
    age=da.year-an-1
print('Vous avez',age,'ans.')
